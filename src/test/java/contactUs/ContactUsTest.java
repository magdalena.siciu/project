package contactUs;

import base.BaseTest;
import base.GeneralElements;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class ContactUsTest extends BaseTest {


    @DataProvider(name = "JSONDataProviderMoreFilesCollection")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFilesFromResources("json/contact");
        for (File f : files){
            ContactUsModel m = mapper.readValue(f, ContactUsModel.class);
            dp.add(new Object[] {m});
        }
        return dp.iterator();
    }

    @Test(dataProvider = "JSONDataProviderMoreFilesCollection" )
    public void loginTest(ContactUsModel contactUsModel){
        driver.get(BASE_URL);
        GeneralElements generalElements = PageFactory.initElements(driver, GeneralElements.class);

        ContactUsPage contactUsPage = PageFactory.initElements(driver, ContactUsPage.class);

        generalElements.getContactUsButton().click();

        contactUsPage.getSubmitButton().click();
        contactUs(contactUsModel);



    }
}
