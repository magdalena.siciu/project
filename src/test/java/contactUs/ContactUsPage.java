package contactUs;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ContactUsPage {

    //obligatory fields

    //email input field
    @FindBy(how = How.ID, using = "yourEmail")
    private WebElement emailInputField;

    //message field
    @FindBy(how = How.ID, using = "message")
    private WebElement messageInputField;

    //non-obligatory fields

    //name field
    @FindBy(how = How.ID, using = "yourName")
    private WebElement nameInputField;

    //subject field
    @FindBy(how = How.ID, using = "subject")
    private WebElement subjectInputField;

    //submit button
    @FindBy(how = How.XPATH, using = "//form[@name='contact_form']//button[@type='submit']")
    private WebElement submitButton;

    //errors
    //email error
    @FindBy(how = How.XPATH, using = "//ul[@id='error_list']//label[@for='yourEmail']")
    private WebElement emailError;

    //message error
    @FindBy(how = How.XPATH, using = "//ul[@id='error_list']//label[@for='message']")
    private WebElement messageError;

    //message sent successfully confirmation
    @FindBy(how = How.CSS, using = "div#flashmessage > a ")
    private WebElement messageSentSuccessfullyConfirmation;

    public WebElement getEmailInputField() {
        return emailInputField;
    }

    public void setEmailInputField(WebElement emailInputField) {
        this.emailInputField = emailInputField;
    }

    public WebElement getMessageInputField() {
        return messageInputField;
    }

    public void setMessageInputField(WebElement messageInputField) {
        this.messageInputField = messageInputField;
    }

    public WebElement getNameInputField() {
        return nameInputField;
    }

    public void setNameInputField(WebElement nameInputField) {
        this.nameInputField = nameInputField;
    }

    public WebElement getSubjectInputField() {
        return subjectInputField;
    }

    public void setSubjectInputField(WebElement subjectInputField) {
        this.subjectInputField = subjectInputField;
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public void setSubmitButton(WebElement submitButton) {
        this.submitButton = submitButton;
    }

    public String getEmailError() {
        return emailError.getText();
    }

    public void setEmailError(WebElement emailError) {
        this.emailError = emailError;
    }

    public String getMessageError() {
        return messageError.getText();
    }

    public void setMessageError(WebElement messageError) {
        this.messageError = messageError;
    }

    public String getMessageSentSuccessfullyConfirmation() {
        return messageSentSuccessfullyConfirmation.getText();
    }

    public void setMessageSentSuccessfullyConfirmation(WebElement messageSentSuccessfullyConfirmation) {
        this.messageSentSuccessfullyConfirmation = messageSentSuccessfullyConfirmation;
    }

    // contact us
    public void contactUs(AccountModelContactUs accountData) {
        nameInputField.clear();
        nameInputField.sendKeys(accountData.getName());

        emailInputField.clear();
        emailInputField.sendKeys(accountData.getEmail());

        subjectInputField.clear();
        subjectInputField.sendKeys(accountData.getSubject());

        messageInputField.clear();
        messageInputField.sendKeys(accountData.getMessage());


        submitButton.click();

    }

}
