package contactUs;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class ContactUsModel {
    AccountModelContactUs account;
    String emailError;
    String messageError;

    @XmlElement
    public void setAccount(AccountModelContactUs account) {
        this.account = account;
    }

    public AccountModelContactUs getAccount() {
        return account;
    }


    public String getEmailError() {
        return emailError;
    }
    @XmlElement
    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getMessageError(){return messageError;}
    @XmlAttribute
    public void setMessageError(String messageError){this.messageError = messageError;}

    public boolean expectSuccessfulContact() {
        if (!this.getEmailError().trim().equalsIgnoreCase("")) {
            return false;
        }

        if (!this.getMessageError().trim().equalsIgnoreCase("")) {
            return false;
        }
        return true;
    }

    public String toString() {
        return String.format("%s, %s", this.account, expectSuccessfulContact());
    }
}
