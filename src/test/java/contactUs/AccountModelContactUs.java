package contactUs;

import javax.xml.bind.annotation.XmlAttribute;

public class AccountModelContactUs {
    public String name;
    public String email;
    public String subject;
    public String message;


    public String getName(){return name;}
    @XmlAttribute
    public void setName(String name){this.name = name;}


    public String getEmail() {
        return email;
    }
    @XmlAttribute
    public void setEmail(String email) {
        this.email = email;
    }


    public String getSubject(){return  subject;}
    @XmlAttribute
    public void setSubject(String subject){this.subject = subject;}

    public String getMessage(){return message;}
    @XmlAttribute
    public void setMessage(String message){this.message = message;}

    public String toString()
    {
        return String.format("n: %s, e: %s, s: %s, m: %s", this.name, this.email, this.subject, this.message);
    }
}
