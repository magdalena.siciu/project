package login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage {

    //input fields

    //email field
    @FindBy(how = How.ID, using = "email")
    private WebElement emailInputField;

    //password field
    @FindBy(how = How.ID, using = "password")
    private WebElement passwordInputFiled;




    //error buttons
    @FindBy(how = How.CSS, using = "div.wrapper.wrapper-flash >div:nth-child(3)")
    private WebElement emailErrorMessage;

    //password error
    @FindBy(how = How.CSS, using = "div.wrapper.wrapper-flash > div:nth-child(4) ")
    private WebElement passwordErrorMessage;



    //buttons

    //remember me
    @FindBy(how = How.ID,using = "remember")
    private WebElement remeberMeButton;

    //login button
    @FindBy (how = How.XPATH, using = "//*[@id=\"main\"]/div/div[2]/form/div[3]/div[2]/button")
    private WebElement loginButton;

    //links

    //register for a new account
    @FindBy(how = How.CSS, using = "div.actions > a:nth-child(1) > a")
    private WebElement registerLink;

    //forgot password
    @FindBy(how = How.LINK_TEXT, using = "Forgot password?")
    private WebElement forgotPasswordLink;

    //username text
    @FindBy(how = How.CSS, using = "li.first.logged > span")
    private WebElement usernameText;

    //getters and setters

    public WebElement getEmailInputField() { return emailInputField; }

    public WebElement getPasswordInputFiled() { return passwordInputFiled; }

    public WebElement getEmailErrorMessage() { return emailErrorMessage; }

    public WebElement getPasswordErrorMessage() { return passwordErrorMessage; }

    public WebElement getRemeberMeButton() { return remeberMeButton; }

    public WebElement getLoginButton() { return loginButton; }

    public WebElement getRegisterLink() { return registerLink; }

    public WebElement getForgotPasswordLink() { return forgotPasswordLink; }

    public void setEmailInputField(WebElement emailInputField) { this.emailInputField = emailInputField; }

    public void setPasswordInputFiled(WebElement passwordInputFiled) { this.passwordInputFiled = passwordInputFiled; }

    public void setEmailErrorMessage(WebElement emailErrorMessage) { this.emailErrorMessage = emailErrorMessage; }

    public void setPasswordErrorMessage(WebElement passwordErrorMessage) { this.passwordErrorMessage = passwordErrorMessage; }

    public void setRemeberMeButton(WebElement remeberMeButton) { this.remeberMeButton = remeberMeButton; }

    public void setLoginButton(WebElement loginButton) { this.loginButton = loginButton; }

    public void setRegisterLink(WebElement registerLink) { this.registerLink = registerLink; }

    public void setForgotPasswordLink(WebElement forgotPasswordLink) { this.forgotPasswordLink = forgotPasswordLink; }

    public WebElement getUsernameText() { return usernameText; }

    public void setUsernameText(WebElement usernameText) { this.usernameText = usernameText; }


    public void login(String email, String password){
        emailInputField.clear();
        emailInputField.sendKeys(email);

        passwordInputFiled.clear();
        passwordInputFiled.sendKeys(password);

        loginButton.click();
    }
}
