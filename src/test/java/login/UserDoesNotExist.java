package login;

import base.BaseTest;
import base.GeneralElements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class UserDoesNotExist extends BaseTest {


    @Test
    public void userDoesNotExist(){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        driver.get("http://siit.atwebpages.com/osc/index.php?page=login");


        loginPage.getEmailInputField().sendKeys("blimp_name@random.com");
        loginPage.getPasswordInputFiled().sendKeys("12345678");
        loginPage.getLoginButton().click();

        WebElement userDoesNotExistMessage = driver.findElement(By.cssSelector("div.wrapper.wrapper-flash > div#flashmessage > a"));

        Assert.assertTrue(userDoesNotExistMessage.isDisplayed());
    }
}
