package login;

import base.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ResetEmail extends BaseTest {

    @Test
    public void resetEmailSuccesful(){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        driver.get("http://siit.atwebpages.com/osc/index.php?page=login");


        loginPage.getForgotPasswordLink().click();

        WebElement emailFieldInput = driver.findElement(By.id("s_email"));
        emailFieldInput.sendKeys("micola.magdalena@yahoo.com");
        WebElement sendPasswordButton = driver.findElement(By.cssSelector("button.ui-button.ui-button-middle.ui-button-main"));
        sendPasswordButton.click();
        WebElement instructionsSentPassword = driver.findElement(By.cssSelector("div.wrapper.wrapper-flash > div#flashmessage"));

        Assert.assertTrue(instructionsSentPassword.isDisplayed());

    }

    @Test
    public void resetEmailUnsuccesful(){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        driver.get("http://siit.atwebpages.com/osc/index.php?page=login");


        loginPage.getForgotPasswordLink().click();

        WebElement emailFieldInput = driver.findElement(By.id("s_email"));
        emailFieldInput.sendKeys("some_email@yahoo.com");
        WebElement sendPasswordButton = driver.findElement(By.cssSelector("button.ui-button.ui-button-middle.ui-button-main"));
        sendPasswordButton.click();
        WebElement unsuccessfulMessage = driver.findElement(By.cssSelector("div.wrapper.wrapper-flash > div.flashmessage.flashmessage-error > a"));

        Assert.assertTrue(unsuccessfulMessage.isDisplayed());

    }
}
