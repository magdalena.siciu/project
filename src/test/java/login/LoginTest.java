package login;

import base.BaseTest;
import base.GeneralElements;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class LoginTest extends BaseTest {



    @DataProvider(name = "loginData")
    public Iterator<Object[]> loginData() {

        Collection<Object[]> dataProvider = new ArrayList<>();

        dataProvider.add(new String[]{"blimp_name@random.com", "123456789"});
        dataProvider.add(new String[]{"my_super_mail@gmail.com", ""});
        dataProvider.add(new String[]{"", "123456789"});
        dataProvider.add(new String[]{"", ""});

        return dataProvider.iterator();

    }

    @Test (dataProvider = "loginData")
    private void loginTestWithDataProvider(String email, String password){
        driver.get(BASE_URL);
        GeneralElements generalElements= PageFactory.initElements(driver, GeneralElements.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);


        generalElements.getLoginButtonTotPage().click();

        loginPage.login(email, password);

        Assert.assertNotNull(loginPage.getEmailErrorMessage());
        Assert.assertNotNull(loginPage.getPasswordErrorMessage());
    }





}
