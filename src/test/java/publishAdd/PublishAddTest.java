package publishAdd;

import base.BaseTest;
import base.GeneralElements;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PublishAddTest extends BaseTest {

    @Test
    public void publishWithTheMinimumReq(){
        driver.get(BASE_URL);
        PublishAddPage publishAddPage = PageFactory.initElements(driver, PublishAddPage.class);
        GeneralElements generalElements= PageFactory.initElements(driver, GeneralElements.class);

        generalElements.getPublishAddButtonTopPage().click();

        WebElement categoryDropdown = publishAddPage.getCategoryDropdown();
        Select selectDropdown = new Select( categoryDropdown);
        selectDropdown.selectByValue("12");

        WebElement regionDropdown = publishAddPage.getRegionDropdown();
        Select selectRegion = new Select( regionDropdown);
        selectRegion.selectByVisibleText("Cluj");

        WebElement cityDropdown = publishAddPage.getCityDropdown();
        Select selectCity = new Select(cityDropdown);
        selectCity.selectByValue("463578");


        publishAddPage.publishAdd("","","","","","","magdalena.siciu@gmail.com");
        publishAddPage.getPublishAddButton().click();
        Assert.assertTrue(publishAddPage.getYourAddWasPublishedMessage().isDisplayed());
    }

    @Test
    public void publishAddWithEveryFieldEmpty(){
        driver.get(BASE_URL);
        PublishAddPage publishAddPage = PageFactory.initElements(driver, PublishAddPage.class);
        GeneralElements generalElements= PageFactory.initElements(driver, GeneralElements.class);

        generalElements.getPublishAddButtonTopPage().click();

        publishAddPage.publishAdd("","","","","","","");
        publishAddPage.getPublishAddButton().click();

        Assert.assertNotNull(publishAddPage.getEmailError());
        Assert.assertNotNull(publishAddPage.getChooseOneCategoryError());
        Assert.assertNotNull((publishAddPage.getSelectARegionError()));

    }

    @Test
    public void publishAddWithEveryFieldCompletedExceptTheObligatory(){
        driver.get(BASE_URL);
        PublishAddPage publishAddPage = PageFactory.initElements(driver, PublishAddPage.class);
        GeneralElements generalElements= PageFactory.initElements(driver, GeneralElements.class);

        generalElements.getPublishAddButtonTopPage().click();

        publishAddPage.publishAdd("Best Dog ever","a realy good dog","53333","Zorilor","Meteor","Batman","");
        publishAddPage.getPublishAddButton().click();

        Assert.assertNotNull(publishAddPage.getEmailError());
        Assert.assertNotNull(publishAddPage.getChooseOneCategoryError());
        Assert.assertNotNull((publishAddPage.getSelectARegionError()));
    }


}
