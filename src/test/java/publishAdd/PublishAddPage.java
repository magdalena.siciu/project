package publishAdd;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PublishAddPage {

    //general information
    @FindBy(how = How.ID, using = "catId")
    private WebElement categoryDropdown;

    @FindBy(how = How.ID, using = "titleen_US")
    private WebElement titleForAdd;

    @FindBy(how = How.ID, using = "descriptionen_US")
    private WebElement descriptionForAdd;

    @FindBy(how = How.ID, using = "price")
    private WebElement priceFieldForAdd;

    @FindBy(how = How.ID, using = "currency")
    private WebElement currencyDropdown;

    @FindBy(how = How.CSS, using = "div#restricted-fine-uploader > div.qq-uploader > div.qq-upload-button")
    private WebElement uploadPicture;

    //listing location
    @FindBy(how = How.ID, using = "regionId")
    private WebElement regionDropdown;

    @FindBy(how = How.ID, using = "cityId")
    private WebElement cityDropdown;

    @FindBy(how = How.ID, using = "cityArea")
    private WebElement cityAreaField;

    @FindBy(how = How.ID, using = "address")
    private WebElement adressField;



    //seller's information
    @FindBy(how = How.ID, using = "contactName")
    private WebElement contactName;

    @FindBy(how = How.ID, using = "contactEmail")
    private WebElement contactEmail;

    //buttons
    @FindBy(how = How.ID, using = "showEmail")
    private WebElement showAddOnListingPage;

    @FindBy(how = How.CSS, using = "fieldset > div:last-of-type > div.controls > button")
    private WebElement publishAddButton;

    //errors and messages
    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(1)")
    private WebElement chooseOneCategoryError;

    @FindBy(how = How.CSS,using = "ul#error_list > li:nth-child(2)")
    private WebElement selectARegionError;

    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(3)")
    private WebElement emailError;

    @FindBy(how = How.ID, using = "flashmessage")
    private WebElement yourAddWasPublishedMessage;

    @FindBy(how = How.CSS, using = "ul.listing-card-list > li:nth-child(1) > div.listing-detail > div.listing-cell > div.listing-data > div.listing-basicinfo > div.listing-attributes > span.currency-value")
    private WebElement assertTheCurrencyChanged;


    public WebElement getAssertTheCurrencyChanged() {
        return assertTheCurrencyChanged;
    }

    public void setAssertTheCurrencyChanged(WebElement assertTheCurrencyChanged) {
        this.assertTheCurrencyChanged = assertTheCurrencyChanged;
    }

    public WebElement getCategoryDropdown() { return categoryDropdown; }

    public void setCategoryDropdown(WebElement categoryDropdown) { this.categoryDropdown = categoryDropdown; }

    public WebElement getTitleForAdd() { return titleForAdd; }

    public void setTitleForAdd(WebElement titleForAdd) { this.titleForAdd = titleForAdd; }

    public WebElement getDescriptionForAdd() { return descriptionForAdd; }

    public void setDescriptionForAdd(WebElement descriptionForAdd) { this.descriptionForAdd = descriptionForAdd; }

    public WebElement getPriceFieldForAdd() { return priceFieldForAdd; }

    public void setPriceFieldForAdd(WebElement priceFieldForAdd) { this.priceFieldForAdd = priceFieldForAdd; }

    public WebElement getCurrencyDropdown() { return currencyDropdown; }

    public void setCurrencyDropdown(WebElement currencyDropdown) { this.currencyDropdown = currencyDropdown; }

    public WebElement getUploadPicture() { return uploadPicture; }

    public void setUploadPicture(WebElement uploadPicture) { this.uploadPicture = uploadPicture; }

    public WebElement getRegionDropdown() { return regionDropdown; }

    public void setRegionDropdown(WebElement regionDropdown) { this.regionDropdown = regionDropdown; }

    public WebElement getCityDropdown() { return cityDropdown; }

    public void setCityDropdown(WebElement cityDropdown) { this.cityDropdown = cityDropdown; }

    public WebElement getCityAreaField() { return cityAreaField; }

    public void setCityAreaField(WebElement cityAreaField) { this.cityAreaField = cityAreaField; }

    public WebElement getAdressField() { return adressField; }

    public void setAdressField(WebElement adressField) { this.adressField = adressField; }

    public WebElement getContactName() { return contactName; }

    public void setContactName(WebElement contactName) { this.contactName = contactName; }

    public WebElement getContactEmail() { return contactEmail;
    }
    public void setContactEmail(WebElement contactEmail) { this.contactEmail = contactEmail; }

    public WebElement getShowAddOnListingPage() { return showAddOnListingPage; }

    public void setShowAddOnListingPage(WebElement showAddOnListingPage) { this.showAddOnListingPage = showAddOnListingPage; }

    public WebElement getPublishAddButton() { return publishAddButton; }

    public void setPublishAddButton(WebElement publishAddButton) { this.publishAddButton = publishAddButton; }

    public WebElement getChooseOneCategoryError() { return chooseOneCategoryError; }

    public void setChooseOneCategoryError(WebElement chooseOneCategoryError) { this.chooseOneCategoryError = chooseOneCategoryError; }

    public WebElement getSelectARegionError() { return selectARegionError; }

    public void setSelectARegionError(WebElement selectARegionError) { this.selectARegionError = selectARegionError; }

    public WebElement getEmailError() { return emailError; }

    public void setEmailError(WebElement emailError) { this.emailError = emailError; }

    public WebElement getYourAddWasPublishedMessage() {
        return yourAddWasPublishedMessage;
    }

    public void setYourAddWasPublishedMessage(WebElement yourAddWasPublishedMessage) {
        this.yourAddWasPublishedMessage = yourAddWasPublishedMessage;
    }



    public void publishAdd(String title, String description, String price, String cityArea, String address, String cotName, String coEmail){
        titleForAdd.clear();
        titleForAdd.sendKeys(title);

        descriptionForAdd.clear();
        descriptionForAdd.sendKeys(description);

        priceFieldForAdd.clear();
        priceFieldForAdd.sendKeys(price);

        cityAreaField.clear();
        cityAreaField.sendKeys(cityArea);

        adressField.clear();
        adressField.sendKeys(address);

        contactName.clear();
        contactName.sendKeys(cotName);

        contactEmail.clear();
        contactEmail.sendKeys(coEmail);
    }
}
