package search.homepage;

import base.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SearchTestOnHomepage extends BaseTest {

    @Test
    public void searchTestOnHomePage(){
        driver.get(BASE_URL);

        SearchPageOnHomepage searchPageOnHomepage = PageFactory.initElements(driver, SearchPageOnHomepage.class);
        searchPageOnHomepage.getInputTestSearch().sendKeys("DZcUDEqU");
        searchPageOnHomepage.getSearchButton().click();

        Assert.assertNotNull(searchPageOnHomepage.getVerifySearchWasSuccessful());



    }

    @Test
    public void searchOfElementWhichDoesNotExist(){
        driver.get(BASE_URL);

        SearchPageOnHomepage searchPageOnHomepage = PageFactory.initElements(driver, SearchPageOnHomepage.class);
        searchPageOnHomepage.getInputTestSearch().sendKeys("dfhnbgiekjguhnkiehngrverhngujre");
        searchPageOnHomepage.getSearchButton().click();
        Assert.assertTrue(searchPageOnHomepage.getEmptySearch().isDisplayed());
    }

    @Test
    public void searchDropdown(){
        driver.get(BASE_URL);
        SearchPageOnHomepage searchPageOnHomepage = PageFactory.initElements(driver, SearchPageOnHomepage.class);

        WebElement searchDropdown = searchPageOnHomepage.getSearchDropdown();
        Select selectDropdown = new Select( searchDropdown);
        selectDropdown.selectByValue("9");
        searchPageOnHomepage.getSearchButton().click();

        WebElement categoryOfSearch = driver.findElement(By.cssSelector("div.resp-wrapper > h1"));

        Assert.assertEquals(searchPageOnHomepage.getSearchResultDescription().getText(), categoryOfSearch.getText());

    }
}
