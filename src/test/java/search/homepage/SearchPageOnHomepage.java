package search.homepage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchPageOnHomepage {

    @FindBy(how = How.CSS, using = "div.main-search > div.cell > div > input ")
    private WebElement inputTestSearch;

    @FindBy(how = How.ID, using = "sCategory")
    private WebElement searchDropdown;

    @FindBy(how = How.CSS, using = "div.main-search > div.cell.reset-padding > button")
    private WebElement searchButton;

    @FindBy(how = How.CSS, using = "ul.listing-card-list > li:first-of-type > div.listing-detail > div.listing-cell > div.listing-data > div.listing-basicinfo > a.title")
    private WebElement verifySearchWasSuccessful;

    @FindBy(how = How.CSS, using = "li.last-child >span")
    private WebElement searchResultDescription;

    @FindBy(how = How.CSS, using = "p.empty")
    private WebElement emptySearch;

    public WebElement getInputTestSearch() {
        return inputTestSearch;
    }

    public void setInputTestSearch(WebElement inputTestSearch) {
        this.inputTestSearch = inputTestSearch;
    }

    public WebElement getSearchDropdown() {
        return searchDropdown;
    }

    public void setSearchDropdown(WebElement searchDropdown) {
        this.searchDropdown = searchDropdown;
    }

    public WebElement getSearchButton() {
        return searchButton;
    }

    public void setSearchButton(WebElement searchButton) {
        this.searchButton = searchButton;
    }

    public WebElement getVerifySearchWasSuccessful() {
        return verifySearchWasSuccessful;
    }

    public void setVerifySearchWasSuccessful(WebElement verifySearchWasSuccessful) {
        this.verifySearchWasSuccessful = verifySearchWasSuccessful;
    }

    public WebElement getSearchResultDescription() {
        return searchResultDescription;
    }

    public void setSearchResultDescription(WebElement searchResultDescription) {
        this.searchResultDescription = searchResultDescription;
    }

    public WebElement getEmptySearch() {
        return emptySearch;
    }

    public void setEmptySearch(WebElement emptySearch) {
        this.emptySearch = emptySearch;
    }
}
