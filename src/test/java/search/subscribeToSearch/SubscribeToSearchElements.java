package search.subscribeToSearch;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SubscribeToSearchElements {


    //subscribe to this search
    @FindBy(how = How.ID, using = "alert_email")
    private WebElement emailField;

    @FindBy(how = How.CSS, using = "form#sub_alert.nocsrf > button")
    private WebElement subscribeButton;

    public WebElement getEmailField() {
        return emailField;
    }

    public void setEmailField(WebElement emailField) {
        this.emailField = emailField;
    }

    public WebElement getSubscribeButton() {
        return subscribeButton;
    }

    public void setSubscribeButton(WebElement subscribeButton) {
        this.subscribeButton = subscribeButton;
    }

    public void subscribeToSearch(String email){
        emailField.clear();
        emailField.sendKeys(email);

        subscribeButton.click();
    }
}
