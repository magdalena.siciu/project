package search.subscribeToSearch;

import base.BaseTest;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import search.homepage.SearchPageOnHomepage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class SubscribeToThisSearchTest extends BaseTest {


    @DataProvider(name = "emailProvider")
    public Iterator<Object[]> emailProvider() {

        Collection<Object[]> dataProvider = new ArrayList<>();

        dataProvider.add(new String[]{"a.b@gmg.com"});
        dataProvider.add(new String[]{"micola.magdalena@yahoo.com"});
        dataProvider.add(new String[]{""});
        dataProvider.add(new String[]{"smth@gmail.com"});

        return dataProvider.iterator();

    }

    @Test
    public void subscribeTest() {
        driver.get(BASE_URL);

        SearchPageOnHomepage searchPageOnHomepage = PageFactory.initElements(driver, SearchPageOnHomepage.class);
        SubscribeToSearchElements subscribeToSearchElements = PageFactory.initElements(driver, SubscribeToSearchElements.class);
        searchPageOnHomepage.getInputTestSearch().sendKeys("Cute Dog");
        searchPageOnHomepage.getSearchButton().click();



        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("");
        arrayList.add("a.b@gmg.com");
        arrayList.add("micola.magdalena@yahoo.com");
        arrayList.add("smth@gmail.com");


        WebDriverWait wait = new WebDriverWait(driver, 3 /*timeout in seconds*/);
        for(String email : arrayList)
        {
           subscribeToSearchElements.subscribeToSearch(email);
           if (wait.until(ExpectedConditions.alertIsPresent()) != null){

               System.out.println("The alert was present for the introduced email " + email);
               Alert alert = driver.switchTo().alert();
               alert.accept();
           } else {
               System.out.println("No alert was displayed for the email " + email);
           }

        }

        }


    }
