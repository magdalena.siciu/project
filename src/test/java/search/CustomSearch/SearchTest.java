package search.CustomSearch;

import base.BaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import search.homepage.SearchPageOnHomepage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class SearchTest extends BaseTest {

    @DataProvider(name = "JSONDataProviderMoreFilesCollection")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFilesFromResources("json/customSearch");
        for (File f : files){
            SearchModel m = mapper.readValue(f, SearchModel.class);
            dp.add(new Object[] {m});
        }
        return dp.iterator();
    }

    @Test(dataProvider = "JSONDataProviderMoreFilesCollection" )
    public void registrationTest(SearchModel searchModel){
        driver.get(BASE_URL);
        CustomSearchPage customSearchPage = PageFactory.initElements(driver, CustomSearchPage.class);
        SearchPageOnHomepage searchPageOnHomepage = PageFactory.initElements(driver, SearchPageOnHomepage.class);

        searchPageOnHomepage.getSearchButton().click();

        //customSearchPage.customSearch(searchModel);


    }
}
