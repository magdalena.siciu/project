package search.CustomSearch;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CustomSearchPage {

    @FindBy(how = How.CSS, using = "form.nocsrf > fieldset.first > div.row > input")
    private WebElement searchTitle;

    @FindBy(how = How.ID, using = "sCity")
    private WebElement city;

    @FindBy(how = How.ID, using = "withPicture")
    private WebElement showAddsWithPicturesOnly;

    @FindBy(how = How.ID, using = "priceMin")
    private WebElement minimumPrice;

    @FindBy(how = How.ID, using = "priceMax")
    private WebElement maximumPrice;

    @FindBy(how = How.LINK_TEXT, using = "All categories")
    private WebElement allCategoriesLink;

    @FindBy(how = How.CSS, using = "div.resp-wrapper  > p.empty")
    private WebElement noElementWasFound;

    @FindBy(how = How.CSS, using = "ul#listing-card-list > li:nth-child(1)")
    private WebElement addFound;

    @FindBy(how = How.CSS, using = "div#sidebar > div.filters > form  > div.actions > button")
    private WebElement submitButton;

    public WebElement getAddFound() {
        return addFound;
    }

    public void setAddFound(WebElement addFound) {
        this.addFound = addFound;
    }

    public WebElement getSearchTitle() {
        return searchTitle;
    }

    public void setSearchTitle(WebElement searchTitle) {
        this.searchTitle = searchTitle;
    }

    public WebElement getCity() {
        return city;
    }

    public void setCity(WebElement city) {
        this.city = city;
    }

    public WebElement getShowAddsWithPicturesOnly() {
        return showAddsWithPicturesOnly;
    }

    public void setShowAddsWithPicturesOnly(WebElement showAddsWithPicturesOnly) {
        this.showAddsWithPicturesOnly = showAddsWithPicturesOnly;
    }

    public WebElement getMinimumPrice() {
        return minimumPrice;
    }

    public void setMinimumPrice(WebElement minimumPrice) {
        this.minimumPrice = minimumPrice;
    }

    public WebElement getMaximumPrice() {
        return maximumPrice;
    }

    public void setMaximumPrice(WebElement maximumPrice) {
        this.maximumPrice = maximumPrice;
    }

    public WebElement getAllCategoriesLink() {
        return allCategoriesLink;
    }

    public void setAllCategoriesLink(WebElement allCategoriesLink) {
        this.allCategoriesLink = allCategoriesLink;
    }

    public WebElement getNoElementWasFound() {
        return noElementWasFound;
    }

    public void setNoElementWasFound(WebElement noElementWasFound) {
        this.noElementWasFound = noElementWasFound;
    }

    public void customSearch(AccountModelSearch accountModelSearch){
        searchTitle.clear();
        searchTitle.sendKeys(accountModelSearch.addName);

        city.clear();
        city.sendKeys(accountModelSearch.city);

        minimumPrice.clear();
        minimumPrice.sendKeys(accountModelSearch.minPrice);

        maximumPrice.clear();
        maximumPrice.sendKeys(accountModelSearch.maxPrice);

        submitButton.click();
    }
}
