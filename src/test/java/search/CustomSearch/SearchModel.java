package search.CustomSearch;

import javax.xml.bind.annotation.XmlElement;

public class SearchModel {
    AccountModelSearch account;
    String searchElementNotFound;


    public String getSearchElementNotFound(){return searchElementNotFound;}
    @XmlElement
    public void setSearchElementNotFound(String searchElementNotFound){this.searchElementNotFound = searchElementNotFound;}

    public void setAccount(AccountModelSearch account) {
        this.account = account;
    }
    @XmlElement
    public AccountModelSearch getAccount() {
        return account;
    }


    public boolean expectAddFound(){
        if (!this.searchElementNotFound.trim().equalsIgnoreCase("")){
            return false;
        }
        return true;
    }

    public String toString() {
        return String.format("%s, %s", this.account, expectAddFound());
    }


}
