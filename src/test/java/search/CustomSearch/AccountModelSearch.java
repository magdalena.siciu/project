package search.CustomSearch;

import javax.xml.bind.annotation.XmlAttribute;

public class AccountModelSearch {
    String addName;
    String city;
    String minPrice;
    String maxPrice;


    public String getAddName(){return addName;}
    @XmlAttribute
    public void setAddName(String addName){this.addName = addName;}

    public String getCity(){return city;}
    @XmlAttribute
    public void setCity (String city){this.city= city;}

    public String getMinPrice(){return minPrice;}
    @XmlAttribute
    public void setMinPrice(String minPrice){this.minPrice=minPrice;}

    public String getMaxPrice(){return maxPrice;}
    @XmlAttribute
    public void  setMaxPrice(String maxPrice){this.maxPrice = maxPrice; }

    public String toString()
    {
        return String.format("u: %s, p: %s", this.addName, this.city, this.minPrice, this.maxPrice);
    }

}
