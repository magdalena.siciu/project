package registration;


import javax.xml.bind.annotation.XmlElement;

public class RegistrationModel {
    AccountModelRegistration account;
    String nameError;
    String emailError;
    String passwordError;
    String repeatPasswordError;




    public String getNameError(){return nameError;}
    @XmlElement
    public void setNameError(String nameError){this.nameError = nameError;}

    public String getEmailError(){return emailError;}
    @XmlElement
    public void setEmailError(String emailError){this.emailError = emailError;}

    public String getPasswordError(){return passwordError;}
    @XmlElement
    public void setPasswordError(String passwordError){this.passwordError = passwordError;}

    public String getRepeatPasswordError(){return repeatPasswordError;}
    @XmlElement
    public void setRepeatPasswordError(String repeatPasswordError){this.repeatPasswordError = repeatPasswordError;}

    @XmlElement
    public void setAccount(AccountModelRegistration account) {
        this.account = account;
    }

    public AccountModelRegistration getAccount() {
        return account;
    }

    public boolean expectSuccessfulRegistration() {
        if (!this.getNameError().trim().equalsIgnoreCase("")) {
            return false;
        }

        if (!this.getEmailError().trim().equalsIgnoreCase("")) {
            return false;
        } if (!this.getPasswordError().trim().equalsIgnoreCase("")){
            return false;
        } if (!this.getRepeatPasswordError().trim().equalsIgnoreCase("")){
            return false;
        }
        return true;
    }

    public String toString() {
        return String.format("%s, %s", this.account, expectSuccessfulRegistration());
    }
}
