package registration;

import base.BaseTest;
import base.GeneralElements;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class RegistrationTest extends BaseTest {

    @DataProvider(name = "JSONDataProviderMoreFilesCollection")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFilesFromResources("json/registration");
        for (File f : files){
            RegistrationModel m = mapper.readValue(f, RegistrationModel.class);
            dp.add(new Object[] {m});
        }
        return dp.iterator();
    }

    @Test(dataProvider = "JSONDataProviderMoreFilesCollection" )
    public void registrationTest(RegistrationModel registrationModel){
       driver.get(BASE_URL);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        GeneralElements generalElements= PageFactory.initElements(driver, GeneralElements.class);

        generalElements.getRegistrationButtonTopPage().click();


        registrationPage.getRegistrationButton().click();
        registration(registrationModel);

    }

}
