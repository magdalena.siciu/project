package registration;

import base.BaseTest;
import base.GeneralElements;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class RegistrationTestEmailAlreadyUsed extends BaseTest {

    @DataProvider(name = "emailAlreadyUsed")
    public Iterator<Object[]> emailAlreadyUsed() {

        Collection<Object[]> dataProvider = new ArrayList<>();

        dataProvider.add(new String[]{"Some Random Name Here", "random_name@random.com", "123456789", "123456789"});

        return dataProvider.iterator();

    }

    @Test(dataProvider = "emailAlreadyUsed")
    public void emailAlreadyUsedTest(String name, String email, String password1, String password2) {
       driver.get(BASE_URL);

        GeneralElements generalElements = PageFactory.initElements(driver, GeneralElements.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        generalElements.getRegistrationButtonTopPage().click();
        registrationPage.emailAlreadyUsed(name, email, password1, password2);

        Assert.assertNotNull(registrationPage.getEmailAlreadyUsed());
    }
}
