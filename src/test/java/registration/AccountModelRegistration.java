package registration;

import javax.xml.bind.annotation.XmlAttribute;

public class AccountModelRegistration {
    String name;
    String email;
    String password;
    String repeatPassword;


    public String getName() { return name; }

    @XmlAttribute
    public void setName(String name) { this.name = name; }

    public String getEmail() {
        return email;
    }

    @XmlAttribute
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword(){return password ;}

    @XmlAttribute
    public void setPassword(String password){this.password = password;}

    public String getRepeatPassword(){return repeatPassword;}
    @XmlAttribute
    public void setRepeatPassword(String repeatPassword){this.repeatPassword = repeatPassword;}



    public String toString()
    {
        return String.format("u: %s, p: %s", this.name, this.email, this.password, this.repeatPassword);
    }
}
