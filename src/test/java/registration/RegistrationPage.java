package registration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class RegistrationPage {
    //name
    @FindBy(how = How.ID, using = "s_name")
    private WebElement nameField;

    //email
    @FindBy(how = How.ID, using = "s_email")
    private WebElement emailField;

    //password
    @FindBy(how = How.ID, using = "s_password")
    private WebElement password1Field;

    //repeat password
    @FindBy(how = How.ID, using = "s_password2")
    private WebElement password2Field;


    //registration button
    @FindBy(how = How.CSS, using = "div.resp-wrapper > form > div:last-child > div.controls > button")
    private WebElement registrationButton;

    //name error
    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(1) > label  ")
    private WebElement nameError;

    //email error
    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(2) > label")
    private WebElement emailError;

    //password 1 error
    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(3) > label")
    private WebElement password1Error;

    //password 2 error
    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(4) > label")
    private WebElement password2Error;

    //succesful registration message
    @FindBy(how = How.CSS, using = "div.wrapper > div#flashmessage.flashmessage.flashmessage-ok")
    private WebElement succesfulRegistrationMessage;

    //email already used
    @FindBy(how = How.CSS, using = "div#flashmessage")
    private WebElement emailAlreadyUsed;



    public WebElement getNameField() {
        return nameField;
    }

    public void setNameField(WebElement nameField) {
        this.nameField = nameField;
    }

    public WebElement getEmailField() {
        return emailField;
    }

    public void setEmailField(WebElement emailField) {
        this.emailField = emailField;
    }

    public WebElement getPassword1Field() {
        return password1Field;
    }

    public void setPassword1Field(WebElement password1Field) {
        this.password1Field = password1Field;
    }

    public WebElement getPassword2Field() {
        return password2Field;
    }

    public void setPassword2Field(WebElement password2Field) {
        this.password2Field = password2Field;
    }

    public WebElement getRegistrationButton() {
        return registrationButton;
    }

    public void setRegistrationButton(WebElement registrationButton) {
        this.registrationButton = registrationButton;
    }

    public String getNameError() {
        return nameError.getText();
    }

    public void setNameError(WebElement nameError) {
        this.nameError = nameError;
    }

    public String getEmailError() {
        return emailError.getText();
    }

    public void setEmailError(WebElement emailError) {
        this.emailError = emailError;
    }

    public String getPassword1Error() { return password1Error.getText(); }

    public void setPassword1Error(WebElement password1Error) {
        this.password1Error = password1Error;
    }

    public String getPassword2Error() {
        return password2Error.getText();
    }

    public void setPassword2Error(WebElement password2Error) {
        this.password2Error = password2Error;
    }

    public String getEmailAlreadyUsed() { return emailAlreadyUsed.getText(); }

    public void setEmailAlreadyUsed(WebElement emailAlreadyUsed) { this.emailAlreadyUsed = emailAlreadyUsed; }

    public WebElement getSuccesfulRegistrationMessage() { return succesfulRegistrationMessage; }

    public void setSuccesfulRegistrationMessage(WebElement succesfulRegistrationMessage) { this.succesfulRegistrationMessage = succesfulRegistrationMessage; }

    public WebElement getSuccessfulRegistrationMessage() {
        return succesfulRegistrationMessage;
    }

    public void setSuccessfulRegistrationMessage(WebElement successfulRegistrationMessage) { this.succesfulRegistrationMessage = successfulRegistrationMessage; }

    //obligatory registration method
    public void registration(AccountModelRegistration accountData) {
        nameField.clear();
        nameField.sendKeys(accountData.getName());

        emailField.clear();
        emailField.sendKeys(accountData.getEmail());

        password1Field.clear();
        password1Field.sendKeys(accountData.getPassword());

        password2Field.clear();
        password2Field.sendKeys(accountData.getRepeatPassword());

        registrationButton.click();
    }

    public void emailAlreadyUsed(String name, String email, String password1, String password2){
        nameField.clear();
        nameField.sendKeys(name);

        emailField.clear();
        emailField.sendKeys(email);

        password1Field.clear();
        password1Field.sendKeys(password1);

        password2Field.clear();
        password2Field.sendKeys(password2);

        registrationButton.click();
    }

}

