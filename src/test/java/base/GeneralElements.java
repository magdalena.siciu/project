package base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class GeneralElements {

    //!!!top of the page elements

    //home page button
    @FindBy(how = How.CSS, using = "#logo > a")
    private WebElement homePageButton;

    //login button
    @FindBy(how = How.ID, using = "login_open")
    private WebElement loginButtonTotPage;

    // TODO find better locator
    //logout button
    @FindBy(how = How.CSS, using = "#header > div.wrapper > ul.nav > li.first.logged > strong > a")
    private WebElement logOutButtonTopPage;

    // TODO find better locator
    //registration button
    @FindBy(how = How.CSS, using = "#header > div.wrapper > ul > li:nth-child(2)")
    private WebElement registrationButtonTopPage;

    // TODO find better locator
    //publish an add
    @FindBy(how = How.CSS, using = "#header > div.wrapper > ul.nav > li.publish")
    private WebElement publishAddButtonTopPage;

    // TODO find better locator
    //my account button
    @FindBy(how = How.CSS, using = "#header > div.wrapper > ul.nav > li.first.logged > strong")
    private WebElement myAccountButton;

    // TODO find better locator
    //welcome user after login
    @FindBy(how = How.CSS, using ="#header > div.wrapper > ul.nav > li.first.logged > span" )
    private WebElement welcomeMessageTopPage;





    //!!!bottom of the page
    // TODO find better locator
    //logout button
    @FindBy(how = How.CSS, using = "#footer > div.wrapper > ul.resp-toggle > li > a:nth-child(2)")
    private WebElement logOutButtonBottomPage;
    // TODO find better locator
    //my account
    @FindBy(how = How.CSS, using = "#footer > div.wrapper > ul.resp-toggle > li > strong")
    private WebElement myAccountBottomPage;
    // TODO find better locator
    //login button
    @FindBy(how = How.CSS, using = "#footer > div.wrapper > ul.resp-toggle > li:nth-child(1) > a")
    private WebElement loginButtonBottomPage;
    // TODO find better locator
    //register button
    @FindBy(how = How.CSS, using = "#footer > div.wrapper > ul.resp-toggle > li:nth-child(2) > a")
    private WebElement registrationButtonBottomPage;
    // TODO find better locator
    //publish an add
    @FindBy(how = How.CSS, using = "#footer > div.wrapper > ul.resp-toggle > li.publish >a")
    private WebElement publishAddButtonBottomPage;
    // TODO find better locator
    //contact us button
    @FindBy(how = How.CSS, using = "#footer > div.wrapper > ul:nth-child(2) > li")
    private WebElement contactUsButton;


    //geters and setters


    public WebElement getHomePageButton() {
        return homePageButton;
    }

    public void setHomePageButton(WebElement homePageButton) {
        this.homePageButton = homePageButton;
    }

    public WebElement getLoginButtonTotPage() {
        return loginButtonTotPage;
    }

    public void setLoginButtonTotPage(WebElement loginButtonTotPage) {
        this.loginButtonTotPage = loginButtonTotPage;
    }

    public WebElement getLogOutButtonTopPage() {
        return logOutButtonTopPage;
    }

    public void setLogOutButtonTopPage(WebElement logOutButtonTopPage) { this.logOutButtonTopPage = logOutButtonTopPage; }

    public WebElement getRegistrationButtonTopPage() {
        return registrationButtonTopPage;
    }

    public void setRegistrationButtonTopPage(WebElement registrationButtonTopPage) { this.registrationButtonTopPage = registrationButtonTopPage; }

    public WebElement getPublishAddButtonTopPage() {
        return publishAddButtonTopPage;
    }

    public void setPublishAddButtonTopPage(WebElement publishAddButtonTopPage) { this.publishAddButtonTopPage = publishAddButtonTopPage; }

    public WebElement getLogOutButtonBottomPage() {
        return logOutButtonBottomPage;
    }

    public void setLogOutButtonBottomPage(WebElement logOutButtonBottomPage) { this.logOutButtonBottomPage = logOutButtonBottomPage; }

    public WebElement getLoginButtonBottomPage() {
        return loginButtonBottomPage;
    }

    public void setLoginButtonBottomPage(WebElement loginButtonBottomPage) { this.loginButtonBottomPage = loginButtonBottomPage; }

    public WebElement getRegistrationButtonBottomPage() {
        return registrationButtonBottomPage;
    }

    public void setRegistrationButtonBottomPage(WebElement registrationButtonBottomPage) { this.registrationButtonBottomPage = registrationButtonBottomPage; }

    public WebElement getPublishAddButtonBottomPage() {
        return publishAddButtonBottomPage;
    }

    public void setPublishAddButtonBottomPage(WebElement publishAddButtonBottomPage) { this.publishAddButtonBottomPage = publishAddButtonBottomPage; }

    public WebElement getContactUsButton() {
        return contactUsButton;
    }

    public void setContactUsButton(WebElement contactUsButton) {
        this.contactUsButton = contactUsButton;
    }

    public WebElement getMyAccountButton() { return myAccountButton; }

    public void setMyAccountButton(WebElement myAccountButton) { this.myAccountButton = myAccountButton; }

    public WebElement getWelcomeMessageTopPage() { return welcomeMessageTopPage; }

    public void setWelcomeMessageTopPage(WebElement welcomeMessageTopPage) { this.welcomeMessageTopPage = welcomeMessageTopPage; }

    public WebElement getMyAccountBottomPage() { return myAccountBottomPage; }

    public void setMyAccountBottomPage(WebElement myAccountBottomPage) { this.myAccountBottomPage = myAccountBottomPage; }
}

