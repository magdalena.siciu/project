package base;

import contactUs.ContactUsModel;
import contactUs.ContactUsPage;
import individualAdd.IndividualAddPage;
import individualAdd.ShareAddModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import registration.RegistrationModel;
import registration.RegistrationPage;
import search.CustomSearch.CustomSearchPage;
import search.CustomSearch.SearchModel;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    public WebDriver driver;
    public String BASE_URL = "http://siit.epizy.com/osc/";

    @BeforeMethod
    public void initDriver() {
        driver = WebBrowser.getDriver(Browser.CHROME);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        if (driver != null) {
            driver.close();
        }
    }

    protected File[] getListOfFilesFromResources(String directoryName) throws UnsupportedEncodingException {
        ClassLoader classLoader = getClass().getClassLoader();
        File directory = new File(URLDecoder.decode(classLoader.getResource(directoryName).getPath(), "UTF-8"));
        File[] files = directory.listFiles();
        System.out.println("Found " + files.length + " files in " + directoryName + " folder");
        return files;
    }

    protected File getFileFromResources(String fileName) throws UnsupportedEncodingException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(URLDecoder.decode(classLoader.getResource(fileName).getFile(), "UTF-8"));
        System.out.println(String.format("Found file %s", file.getName()));
        return file;
    }


    protected void registration(RegistrationModel registrationModel) {
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.registration(registrationModel.getAccount());


        if (registrationModel.expectSuccessfulRegistration()) {
            Assert.assertNotNull(registrationPage.getSuccessfulRegistrationMessage());
        } else {
            Assert.assertEquals(registrationPage.getNameError(), registrationModel.getNameError());
            Assert.assertEquals(registrationPage.getEmailError(), registrationModel.getEmailError());
            Assert.assertEquals(registrationPage.getPassword1Error(), registrationModel.getPasswordError());
            Assert.assertEquals(registrationPage.getPassword2Error(), registrationModel.getRepeatPasswordError());
        }
    }

    protected void contactUs(ContactUsModel contactUsModel) {
        ContactUsPage contactUsPage = PageFactory.initElements(driver, ContactUsPage.class);


        contactUsPage.contactUs(contactUsModel.getAccount());

        if (contactUsModel.expectSuccessfulContact()) {
            Assert.assertNotNull(contactUsPage.getMessageSentSuccessfullyConfirmation());
        } else {
            Assert.assertEquals(contactUsPage.getEmailError(), contactUsModel.getEmailError());
            Assert.assertEquals(contactUsPage.getMessageError(), contactUsModel.getMessageError());
        }

    }


    protected void shareAdd(ShareAddModel shareAddModel) {
        IndividualAddPage individualAddPage = PageFactory.initElements(driver, IndividualAddPage.class);
        individualAddPage.share(shareAddModel.getAccount());

        if (shareAddModel.expectSuccessfulShare()) {
            Assert.assertNotNull(individualAddPage.getMessageSuccessfullySent());
        } else {
            Assert.assertNotNull(individualAddPage.getNameError());
            Assert.assertNotNull(individualAddPage.getEmailError());
            Assert.assertNotNull(individualAddPage.getFriendNameError());
            Assert.assertNotNull(individualAddPage.getFriendEmailError());
            Assert.assertNotNull(individualAddPage.getMessageError());
        }
    }

    protected void customSearch(SearchModel searchModel) {
        CustomSearchPage customSearchPage = PageFactory.initElements(driver, CustomSearchPage.class);

        customSearchPage.customSearch(searchModel.getAccount());
        if (searchModel.expectAddFound()) {
            Assert.assertNotNull(customSearchPage.getAddFound());
        } else {
            Assert.assertNotNull(customSearchPage.getNoElementWasFound());
        }
    }

}
