package individualAdd;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class IndividualAddPage {

    //main photo
    @FindBy(how = How.CSS, using = "div.item-photos >a.main-photo")
    private WebElement mainPhoto;

    @FindBy(how = How.CSS, using = "a.fancybox-item.fancybox-close")
    private WebElement closeMaximizedPhoto;

    @FindBy(how = How.CSS, using = "span.price")
    private WebElement verifyThePictureIsNoLongerMaximized;

    public WebElement getMainPhoto() {
        return mainPhoto;
    }

    public void setMainPhoto(WebElement mainPhoto) {
        this.mainPhoto = mainPhoto;
    }

    public WebElement getCloseMaximizedPhoto() {
        return closeMaximizedPhoto;
    }

    public void setCloseMaximizedPhoto(WebElement closeMaximizedPhoto) {
        this.closeMaximizedPhoto = closeMaximizedPhoto;
    }

    public WebElement getVerifyThePictureIsNoLongerMaximized() {
        return verifyThePictureIsNoLongerMaximized;
    }

    public void setVerifyThePictureIsNoLongerMaximized(WebElement verifyThePictureIsNoLongerMaximized) {
        this.verifyThePictureIsNoLongerMaximized = verifyThePictureIsNoLongerMaximized;
    }



    //send to a friend

    @FindBy(how = How.LINK_TEXT, using = "Share")
    private WebElement shareThisAddButton;

    @FindBy(how = How.ID, using = "yourName")
    private WebElement yourName;

    @FindBy(how = How.ID, using = "yourEmail")
    private WebElement yourEmail;

    @FindBy (how = How.ID, using = "friendName")
    private WebElement friendName;

    @FindBy (how = How.ID, using = "friendEmail")
    private WebElement friendEmail;

    @FindBy(how = How.ID, using = "subject")
    private WebElement subject;

    @FindBy(how = How.ID, using = "message")
    private WebElement message;

    @FindBy(how = How.CSS, using = "form > div:last-of-type > div.controls > button")
    private WebElement sendMessage;

    //messages
    @FindBy(how = How.CSS, using = "div#flashmessage.flashmessage.flashmessage-ok")
    private WebElement messageSuccessfullySent;

    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(1)")
    private WebElement nameError;

    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(2)")
    private WebElement emailError;

    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(3)")
    private WebElement friendNameError;

    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(4)")
    private WebElement friendEmailError;

    @FindBy(how = How.CSS, using = "ul#error_list > li:nth-child(5)")
    private WebElement messageError;

    public WebElement getShareThisAddButton() { return shareThisAddButton; }

    public void setShareThisAddButton(WebElement shareThisAddButton) { this.shareThisAddButton = shareThisAddButton; }

    public WebElement getYourName() { return yourName; }

    public void setYourName(WebElement yourName) { this.yourName = yourName; }

    public WebElement getYourEmail() { return yourEmail; }

    public void setYourEmail(WebElement yourEmail) { this.yourEmail = yourEmail; }

    public WebElement getFriendName() { return friendName; }

    public void setFriendName(WebElement friendName) { this.friendName = friendName; }

    public WebElement getFriendEmail() { return friendEmail; }

    public void setFriendEmail(WebElement friendEmail) { this.friendEmail = friendEmail; }

    public WebElement getSubject() { return subject; }

    public void setSubject(WebElement subject) { this.subject = subject; }

    public WebElement getMessage() { return message; }

    public void setMessage(WebElement message) { this.message = message; }

    public WebElement getSendMessage() { return sendMessage; }

    public void setSendMessage(WebElement sendMessage) { this.sendMessage = sendMessage; }

    public WebElement getMessageSuccessfullySent() { return messageSuccessfullySent; }

    public void setMessageSuccessfullySent(WebElement messageSuccessfullySent) { this.messageSuccessfullySent = messageSuccessfullySent; }

    public WebElement getNameError() { return nameError; }

    public void setNameError(WebElement nameError) { this.nameError = nameError; }

    public WebElement getEmailError() { return emailError; }

    public void setEmailError(WebElement emailError) { this.emailError = emailError; }

    public WebElement getFriendNameError() { return friendNameError; }

    public void setFriendNameError(WebElement friendNameError) { this.friendNameError = friendNameError; }

    public WebElement getFriendEmailError() { return friendEmailError; }

    public void setFriendEmailError(WebElement friendEmailError) { this.friendEmailError = friendEmailError; }

    public WebElement getMessageError() { return messageError; }

    public void setMessageError(WebElement messageError) { this.messageError = messageError; }

    //method
    public void share(AccountModelShareAdd accountData){
        yourName.clear();
        yourName.sendKeys(accountData.getYourName());

        yourEmail.clear();
        yourName.sendKeys(accountData.getYourEmail());

        friendName.clear();
        friendName.sendKeys(accountData.getFriendName());

        friendEmail.clear();
        friendEmail.sendKeys(accountData.getFriendEmail());

        subject.clear();
        subject.sendKeys(accountData.getSubject());

        message.clear();
        message.sendKeys(accountData.getMessage());

        sendMessage.click();
    }

}
