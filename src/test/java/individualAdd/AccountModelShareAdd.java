package individualAdd;

import javax.xml.bind.annotation.XmlAttribute;

public class AccountModelShareAdd {
    public String yourName;
    public String yourEmail;
    public String friendName;
    public String friendEmail;
    public String subject;
    public String message;

    public String getYourName(){return yourName;}
    @XmlAttribute
    public void setYourName(String yourName){this.yourName = yourName;}

    public String getYourEmail(){return yourEmail;}
    @XmlAttribute
    public void setYourEmail(String yourEmail){this.yourEmail = yourEmail;}

    public String getFriendName(){return friendName;}
    @XmlAttribute
    public void setFriendName(String friendName){this.friendName = friendName;}

    public String getFriendEmail(){return friendEmail;}
    @XmlAttribute
    public void setFriendEmail(String friendEmail){this.friendEmail = friendEmail;}

    public String getSubject(){return subject;}
    @XmlAttribute
    public void setSubject(String subject){this.subject = subject;}

    public String getMessage(){return message;}
    @XmlAttribute
    public void setMessage(String message){this.message = message;}

    public String toString()
    {
        // TODO add missing formats
        return String.format("u: %s, p: %s", this.yourName, this.yourEmail, this.friendName, this.friendEmail, this.subject, this.message);
    }
}
