package individualAdd;

import javax.xml.bind.annotation.XmlElement;

public class ShareAddModel {
    AccountModelShareAdd account;
    String yourNameError;
    String yourEmailError;
    String friendNameError;
    String friendEmailError;
    String messageError;

    public String getYourNameError(){return yourNameError;}
    @XmlElement
    public void setYourNameError(String yourNameError){this.yourNameError = yourNameError;}

    public String getYourEmailError(){return yourEmailError;}
    @XmlElement
    public void setYourEmailError(String yourEmailError){this.yourEmailError = yourEmailError;}

    public String getFriendNameError(){return friendNameError;}
    @XmlElement
    public void setFriendNameError(String friendNameError){this.friendNameError = friendNameError;}

    public String getFriendEmailError(){return friendEmailError;}
    @XmlElement
    public void setFriendEmailError(String friendEmailError){this.friendEmailError = friendEmailError;}

    public String getMessageError(){return messageError;}
    @XmlElement
    public void setMessageError(String messageError){this.messageError = messageError;}





    @XmlElement
    public void setAccount(AccountModelShareAdd account) {
        this.account = account;
    }

    public AccountModelShareAdd getAccount() {
        return account;
    }



    public boolean expectSuccessfulShare() {
        if (!this.getYourNameError().trim().equalsIgnoreCase("")) {
            return false;
        }
        if (!this.getYourEmailError().trim().equalsIgnoreCase("")) {
            return false;
        } if (!this.getFriendNameError().trim().equalsIgnoreCase("")){
            return false;
        } if (!this.getFriendEmailError().trim().equalsIgnoreCase("")){
            return false;
        }if (!this.getMessageError().trim().equalsIgnoreCase("")){
            return false;
        }return true;
    }

   // public String toString() {
     //   return String.format("%s, %s", this.account, expectSuccessfulShare());
    //}

}
