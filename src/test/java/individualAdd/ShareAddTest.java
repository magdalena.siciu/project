package individualAdd;

import base.BaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class ShareAddTest extends BaseTest {


    @DataProvider(name = "JSONDataProviderMoreFilesCollection")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFilesFromResources("json/shareAdd");
        for (File f : files){
            ShareAddModel m = mapper.readValue(f, ShareAddModel.class);
            dp.add(new Object[] {m});
        }
        return dp.iterator();
    }


    @Test (dataProvider = "JSONDataProviderMoreFilesCollection")
    public void shareTest(ShareAddModel shareAddModel) throws InterruptedException {
        driver.get(BASE_URL);


        WebElement randomAddOnFirstPage = driver.findElement(By.cssSelector("ul#listing-card-list >li:nth-child(1) > a"));
        randomAddOnFirstPage.click();

        IndividualAddPage individualAddPage= PageFactory.initElements(driver, IndividualAddPage.class);
        individualAddPage.getShareThisAddButton().click();

        individualAddPage.getSendMessage().click();
       shareAdd(shareAddModel);

    }
}
