package individualAdd;

import base.BaseTest;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PhotoTest extends BaseTest {

    @Test
    public void photoTest(){
        IndividualAddPage individualAddPage = PageFactory.initElements(driver, IndividualAddPage.class);

        driver.get(BASE_URL + "index.php?page=item&id=368");

        individualAddPage.getMainPhoto().click();
        individualAddPage.getCloseMaximizedPhoto().click();

        Assert.assertNotNull(individualAddPage.getVerifyThePictureIsNoLongerMaximized());


    }

}
